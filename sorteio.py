import pandas as pd
import random
import tkinter as tk
from tkinter import filedialog, messagebox
from datetime import datetime

def sortear_usuario():
    try:
        file_path = filedialog.askopenfilename(title="Selecione o arquivo Excel", filetypes=[("Arquivos Excel", "*.xlsx;*.xls")])

        if not file_path:
            return

        planilha = pd.read_excel(file_path)
        nomes = planilha['RedditUser'].tolist()
        usuario_sorteado = random.choice(nomes)

        data_hora_sorteio = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        with open("resultado_sorteio.txt", "a") as arquivo:
            arquivo.write(f"Vencedor: {usuario_sorteado}, Data e Hora do Sorteio: {data_hora_sorteio}\n")

        mensagem = f"Parabéns, {usuario_sorteado}! Você ganhou o sorteio!"
        messagebox.showinfo("Resultado do Sorteio", mensagem)

    except Exception as e:
        messagebox.showerror("Erro", f"Ocorreu um erro: {str(e)}")

janela = tk.Tk()
janela.title("Sorteio")

botao = tk.Button(janela, text="Sortear Usuário", command=sortear_usuario)
botao.pack(padx=20, pady=20)

janela.mainloop()